## Simple JS/Node Developer Challenge

### Goal
- This repo is for build a simple dictionary key/value store script using only core NodeAPI and ECMAScript 6.  
- Store the key/value dictionary using filesystem.

### Store Commands

- Using node command

`$ node store.js add mykey myvalue`

`$ node store.js list`

`$ node store.js get mykey`

`$ node store.js remove mykey`

`$ node store.js clear`

- using npm

`$ npm run start add mykey myvalue`

`$ npm run start list`

`$ npm run start get mykey`

`$ npm run start remove mykey`

`$ npm run start clear`

