const fs = require('fs');
const path = require('path');

const fileName = 'store-data.json';
const file_path = path.join(__dirname, fileName);

const checkFileExists = () => fs.existsSync(file_path);

const createFile = () => fs.appendFileSync(file_path, '{}');

const fetchStore = () => {
    try {
        const data = fs.readFileSync(fileName);
        return JSON.parse(data);
    } catch (e) {
        return {};
    }
};

const saveStore = (items) => {
    fs.writeFileSync('store-data.json', JSON.stringify(items));
};

const getAll = () => {
    return fetchStore();
};

const getSingle = (key) => {
    const stores = fetchStore();
    return stores[key];
};

const add = (key, value) => {
    const stores = fetchStore();
    if (stores[key]) {
        return console.log('This key is already exists');
    } else {
        stores[key] = value;
        saveStore(stores);
        return console.log(`key: ${key}, value: ${value}`);
    }
};

const remove = (key) => {
    const stores = fetchStore();
    if (stores[key]) {
        delete(stores[key]);
    } else {
        return console.log('key does not exist')
    }
};

const clear = () => {
    saveStore({});
};

module.exports = {
    getAll,
    getSingle,
    add,
    remove,
    clear,
    checkFileExists,
    createFile
};
