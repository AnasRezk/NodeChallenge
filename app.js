const store = require('./store.js');
const readline = require('readline');

if (store.checkFileExists()) {
    switch (process.argv[2]) {
        case 'list' :
            console.log(store.getAll());
            break;
        case 'get' :
            console.log(store.getSingle(process.argv[3]));
            break;
        case 'add' :
            store.add(process.argv[3], process.argv[4]);
            break;
        case 'remove' :
            store.remove(process.argv[3]);
            break;
        case 'clear' :
            store.clear();
            break;
        default:
            console.log('Unknown input');
    }
} else {
    console.log('file not found');
    const rl = readline.createInterface({
        input: process.stdin,
        output: process.stdout
    });

    rl.question('Do you want to create new one [y/n]? ', (answer) => {
        if (answer.toLowerCase() === 'y') {
            store.createFile();
        }
        rl.close();
    });
}